package com.example.myusers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.myusers.models.user_name;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<user_name> user_number;
    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv = findViewById(R.id.user_rv);
        user_number = new ArrayList<>();

        setUserInfo();
        setUserAdapter();
    }

     private void setUserInfo() {

        user_number.add(new user_name("Ahror","+998 334172525","student","5600 so'm"));
        user_number.add(new user_name("Abror","+998 9083839363","student","50600 so'm"));
        user_number.add(new user_name("Hamidullo","+998 911234567","programmist","65000 so'm"));
        user_number.add(new user_name("Zayniddin","+998 976270308","student","15600 so'm"));
        user_number.add(new user_name("Izatillo","+998 901325808","programmist","900 so'm"));
        user_number.add(new user_name("Ahrorbek","+998 334172525","student","5000 so'm"));

    }

    private void setUserAdapter() {
        UserAdapter adapter = new UserAdapter(user_number);
        RecyclerView.LayoutManager lm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(lm);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
    }


}