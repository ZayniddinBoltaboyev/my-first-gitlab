package com.example.myusers.models;

public class user_name {

    private String Name;
    private  String Number;
    private  String Job;
    private  String Summa;

    public user_name(String name, String number, String job, String summa) {
        this.Name = name;
        this.Number = number;
        this.Job = job;
        this.Summa = summa;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getJob() {
        return Job;
    }

    public void setJob(String job) {
        Job = job;
    }

    public String getSumma() {
        return Summa;
    }

    public void setSumma(String summa) {
        Summa = summa;
    }
}
